"use strict";

function modal__open(constantes) {
    showHide(constantes.CONTAINER, true);
}

function modal__close(constantes) {
    showHide(constantes.CONTAINER, false);
}

export function events(constantes) {
    constantes.CLOSE.addEventListener(
        "click",
        function () { modal__close(constantes) },
        false
    );
}

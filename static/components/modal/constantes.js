"use strict";

export let CONTAINER = null;
export let CONTENT = null;
export let TITLE = null;
export let BODY = null;
export let CLOSE = null;

export let TITLE_SIDE = null;
export let CONTENT_SIDE = null;
export let CLOSE_BUTTON_SIDE = null;

export function init() {
    CONTAINER = getClassEl('component-modal__container');
    CONTENT = document.getElementById('component-modal-content');
    TITLE = getClassEl('component-modal__title');
    BODY = getClassEl('component-modal__body');
    CLOSE = getClassEl('component-modal__close');

    TITLE_SIDE = document.getElementById('component-modal-title');
    CONTENT_SIDE = document.getElementById('component-modal-content');
    CLOSE_BUTTON_SIDE = document.getElementById('component-modal-show-close-button');
}

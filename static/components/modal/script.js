"use strict";

export function prefill(constantes, content) {
    $(constantes.CONTENT_SIDE).trumbowyg({
        lang: 'fr',
        btns: [
            ['viewHTML'],
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['base64'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat']
        ]
    });

    let _title = content.title;
    if (!content.title)
        _title = '';
    constantes.TITLE_SIDE.value = _title;

    let _content = '';
    if (content.content)
        _content = content.content;
    constantes.CONTENT_SIDE.innerHTML = _content;
    constantes.CLOSE_BUTTON_SIDE.checked = content.show_close_button;
}

export function preview(constantes, component, content) {
    _apply_modal_title(constantes.TITLE, content.title);
    _apply_modal_content(constantes.BODY, content.content);
    _apply_modal_close_button(constantes.CLOSE, content.show_close_button);
    _apply_modal_footer(constantes, content.footer);
    component.events(constantes);
}

export function apply_component(constantes) {
    _apply_modal_title(constantes.TITLE, constantes.TITLE_SIDE.value);
    _apply_modal_content(constantes.BODY, constantes.CONTENT_SIDE.innerHTML);
    _apply_modal_close_button(constantes.CLOSE, constantes.CLOSE_BUTTON_SIDE.checked);
    _apply_modal_footer(constantes);
}

function _apply_modal_title(modal_title, modalTitleValue) {
    modal_title.innerText = modalTitleValue;
    showHide(modal_title, modalTitleValue);
}

function _apply_modal_content(modal_body, content) {
    let _content = '';
    if (content)
        _content = content;
    modal_body.innerHTML = _content;
}

function _apply_modal_close_button(close_button, showCloseButton) {
    showHide(close_button, showCloseButton);
}

function _apply_modal_footer(constantes, footer) {
    if (!footer)
        return
    Object.keys(footer).map(function (value) { debugger; });
}

